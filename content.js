// content.js
chrome.runtime.onMessage.addListener(
    function(request, sender, sendResponse) {
      if( request.message === "clicked_browser_action" ) {
        var firstHref = $("a[href^='http']").eq(0).attr("href");
  
        console.log("Send to Mopidy: ", firstHref);
        
        jbody=''.concat('{"method": "core.tracklist.add", "jsonrpc": "2.0", "params": { "uri": "yt:', firstHref, '" }, "id": 1 }')
        
        var xhr = new XMLHttpRequest();
        xhr.open("POST",  'https://retropie.lan/mopidy/rpc', true);
        xhr.setRequestHeader('Content-Type', 'application/json');
        xhr.setRequestHeader('Access-Control-Allow-Origin', '*');
        xhr.send(jbody);
        console.log(jbody);
        //chrome.runtime.sendMessage({"message": "open_new_tab", "url": firstHref});
      }
    }
  );
